This repository contains code for generating the last batch of happy and sad mood channel classifiers. 

The code is written in python and requires numpy, scypy, and scikit-learn installed to run.

There are two files, main.py and main_demo.py -- the two are essentially identical, only that main is written to classify the original dreamhost tracks and main_demo classifies the 1million tracks from 7digital. The pre-processing and learning procedures are the same between them and documentation focuses on that.

Additionally, the folder final_demo is also the same, but classifies the tracks for the final demo using Dreamhost tracks and continues the php file used to create the online demo.

A more complete description of the procedure for learning is available on the wiki.
https://sourcetone.wikidot.com/sad-classifier-demo

Note: for the sake of space, the actual feature csv files of the 1M demo tracks are not included. Instead, the SQL query which generates it is included, and that can be used to recompile the feature files.