import re
import csv
import numpy as np
from sklearn import preprocessing
#from scikits.learn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
import random
from sklearn import svm


# DREAMHOST:
# SELECT f.*, s.valence, s.arousal, s.audiofiles_id, sg.genre, sg.subgenre, sg.year, CONVERT(af.path USING UTF8) 
# FROM ratemood_development.feature_vectors2 as f
# INNER JOIN ratemood_development.songvectors2 as s ON
# f.songvectors_id = s.id
# INNER JOIN ratemood_development.audiofiles as af
# ON af.id = s.audiofiles_id
# INNER JOIN ratemood_development.songs as sg
# ON af.id = sg.audiofiles_id
# WHERE f.mMFCC1 IS NOT NULL
# LIMIT 2000000000	
def get_test_set():
	test_tracks = []
	test_features = []
	tracks = csv.reader(open('/Users/Gene/sourcetone/trainSad/final_demo/data/test_tracks.csv', 'r'), delimiter=',')	
	tracks.next()
	for track in tracks:
		sv_id = int(track[0])
		feat = track[1:161]
		for i in range(0, len(feat)):	feat[i] = float(feat[i])
		val = float(track[161])
		aro = float(track[162])
		path_id = int(track[163])
		gen = track[164]
		subgen = track[165]
		if (track[166]=='NULL'):	year = 'NULL'
		else:						year = int(track[166])
		url = re.sub('/home/sourcetone/mp3lib', 'http://admin.sourcetone.com/songs_mp3', track[167])
		test_tracks.append([url, sv_id, gen, subgen, val, aro, year])
		test_features.append(feat)
	test_features = np.array(test_features)	
	return (test_tracks, test_features)	

def get_ground_truth(classifier):
	features = {}		# get features first
	#feat_gt = csv.reader(open(path+'original_features.csv', 'r'), delimiter=',')
	feat_gt = csv.reader(open('/Users/Gene/sourcetone/trainSad/final_demo/data/original_features.csv', 'r'), delimiter=',')	
	feat_gt.next()
	for track in feat_gt:
		sv_id = int(track[0])
		feat = track[1:]
		for i in range(0, len(feat)):	feat[i] = float(feat[i])
		features[int(sv_id)] = feat	
	gt_tracks = []		# get current results and append features if available
	gt_features = []
	#results = csv.reader(open(path+'ground_truth.csv', 'r'), delimiter=',')
	results = csv.reader(open('/Users/Gene/sourcetone/trainSad/final_demo/data/ground_truth.csv', 'r'), delimiter=',')
	results.next()
	for (idx, songvectors_id, audio_path, classifier_round, classifier0, prediction, result, labeler) in results:
		audio_path = re.sub('/home/sourcetone/mp3lib', 'http://admin.sourcetone.com/songs_mp3', audio_path)
		if classifier0==classifier:
			try:
				feat = features[int(songvectors_id)]
				if result=='true' or result=='false':
					gt_tracks.append([int(songvectors_id), int(classifier_round), classifier0, result, labeler])
					gt_features.append(feat)
			except:	pass
	return (gt_tracks, gt_features)

def normalize_features(feat1, feat2):	
	feat = np.concatenate([feat1, feat2])
	#feat_norm = preprocessing.normalize(feat, norm='l2')
	for i in range(feat.shape[1]):
		feat[:,i] = (feat[:,i]-np.min(feat[:,i]))/(np.max(feat[:,i])-np.min(feat[:,i]))
	feat1 = feat[0:len(feat1)]
	feat2 = feat[len(feat1):]
	return (feat1, feat2)
	
def train_logistic_regression(tracks, features):
	labels = []
	for (sv_id, classifier_round, classifier, result, labeler) in tracks:
		if result=='true':		labels.append(1)
		elif result=='false':	labels.append(0)
	labels = np.array(labels)
	param_grid = {'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000] }
	clf = GridSearchCV(LogisticRegression(penalty='l2'), param_grid)
	clf = clf.fit(features, labels)
	print "Best estimator found by grid search:"
	print clf.best_estimator_
	return clf


def write_all_sql_tracks(filename, tracks, prob_sad, prob_happy):
	writefile = open(filename,'w')
	skipped = 0
	for i in range(0, len(tracks)):
		(url, sv_id, genre, subgenre, val, aro, year) = tracks[i]
		if url.find('"') == -1:
			if genre=='Hip Hop':	genre='HipHop'
			if genre=='New Age':	genre='NewAge'
			if genre=='Pop+Rock':	genre='PopRock'			
			if genre!='NULL':		genre = '"'+genre+'"'
			if subgenre!='NULL':	subgenre = '"'+subgenre+'"'
			ps = prob_sad[i][1]
			ph = prob_happy[i][1]
			values = str(sv_id)+','+genre+','+subgenre+','+str(val)+','+str(aro)+','+str(year)+',"'+url+'",'+str(ps)+','+str(ph)
			query = 'INSERT INTO ratemood_development.classifier_sad_happy (songvectors_id, genre, subgenre, valence, arousal, year, path, sad, happy) VALUES ('+values+');'
			writefile.write(query+'\n')
		else:
			print "nope"
			skipped+=1
	print "skipped "+str(skipped)

# sad
(gt_tracks, gt_features) = get_ground_truth('sad')
(test_tracks, test_features) = get_test_set()
(gt_features, test_features) = normalize_features(gt_features, test_features)
classifier = train_logistic_regression(gt_tracks, gt_features)
prob_sad = classifier.predict_proba(test_features)

# happy
(gt_tracks, gt_features) = get_ground_truth('happy')
(test_tracks, test_features) = get_test_set()
(gt_features, test_features) = normalize_features(gt_features, test_features)
classifier = train_logistic_regression(gt_tracks, gt_features)
prob_happy = classifier.predict_proba(test_features)

write_all_sql_tracks('/Users/Gene/Desktop/pred_sql.txt', test_tracks, prob_sad, prob_happy)
