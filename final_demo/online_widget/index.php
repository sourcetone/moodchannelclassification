<?php session_start(); ?>

<html>
<head>
	<title>Sourcetone sad/happy classifier</title>
</head>
<body>

<p>&nbsp;&nbsp;
<p>

<?php

$genreNames = array( "Electronic", "PopRock", "Gospel", "Blues", "Soundtrack", 
				 	 "World", "HipHop", "R&B", "Latin", "Country", 
				 	 "Jazz", "NewAge", "Reggae", "Classical" );

$track = lookupTracks($genreNames);
if (!is_null($track)) 
	displayMp3($track, $_POST['classifier']);
displaySelectionMenu($genreNames);
?>

</body>
</html>

<?php

function displayMp3($track, $classifier) 
{
	$sv_id = $track[0];
	$genre = $track[1];
	$subgenre = $track[2];
	$val = $track[3];
	$aro = $track[4];
	$url = $track[5];
	$sad = $track[6];
	$happy = $track[7];
	$year = $track[8];
	$trackname = substr($url, 38, -4);
	$trackname = str_replace("/", " / ", $trackname);
	if ($classifier=='Sad')			{ $bgcolor = "#00F"; }
	else if ($classifier=='Happy')	{ $bgcolor = "#F00"; }
	?>	

	<script type="text/javascript">
	function Reload () {
		var f = document.getElementById('mp3link');
		f.src = f.src;
	}
	</script>

	<center>
	<table width="800" border=0>
		<tr>
			<td width="80%" bgcolor="<?php echo $bgcolor ?>">
				<h4><font color="#FFF">
					<br/><center>
					<?php echo $trackname; ?><br/>
					<b>Genre : </b><?php echo $genre; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<b>Subgenre : </b><?php echo $subgenre; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<b>Year : </b><?php echo $year; ?>
					<br/>
					<b>Sad : </b><?php echo $sad; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<b>Happy : </b><?php echo $happy; ?><br/>					
					<b>Valence : </b><?php echo $val; ?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<b>Arousal : </b><?php echo $aro; ?><br/>
					</center>
				</h4>
			</td>
		</tr>
		<tr>
			<td colspan=2>
				<center>
				<iframe id="mp3link" src="<?php echo $url; ?>" width="100%" height="110" frameborder="0" scrolling="no"></iframe>
				<br/>
				<input type="button" value="If mp3 not loading, click here to refresh it" onclick="Reload();">
				</center>
			</td>
		</tr>
	</table>
	<p>	
	<?php	
}

function lookupTracks($genreNames) {
	$genresSQL = array();
	$nGenres = 0;
	for ($i=0; $i<count($genreNames); $i++) {
		if (isset($_POST[$genreNames[$i]])) {
			$genresSQL[] = '"'.$genreNames[$i].'"';
			$nGenres++;
		}
	}
	$minyear = $_POST['minyear'];
	$maxyear = $_POST['maxyear'];
	$minprob = $_POST['minprob'];
	$limit = 500; //$_POST['limit'];
	$classifier = $_POST['classifier'];
	
	if ($classifier=='Sad')
		$sqlQuery = 'SELECT * FROM ratemood_development.classifier_sad_happy WHERE sad > '.$minprob;	
	else if ($classifier=='Happy')
		$sqlQuery = 'SELECT * FROM ratemood_development.classifier_sad_happy WHERE happy >'.$minprob;	
	else return NULL;
	
	if ($nGenres>0) 
		$sqlQuery = $sqlQuery . ' AND genre IN ('.implode(',',$genresSQL).')';
	if ($minyear != '' && $maxyear != '') {
		if ($nGenres > 0)
			$sqlQuery = $sqlQuery . ' AND year BETWEEN '.$minyear.' AND '.$maxyear;
		else
			$sqlQuery = $sqlQuery . ' AND year BETWEEN '.$minyear.' AND '.$maxyear;
	}

	if ($classifier=='Sad')			{ $sqlQuery = $sqlQuery . " ORDER BY sad"; }
	else if ($classifier=='Happy')	{ $sqlQuery = $sqlQuery . " ORDER BY happy"; }
	$track = getTrack($sqlQuery, $classifier, $limit, $genreNames);
	return $track;
}

function displaySelectionMenu($genreNames) {
	$theMinScore = '0.8';
	if (isset($_POST['minprob']))	$theMinScore = $_POST['minprob'];
	?>
	<center>
		<table width="800" border=0 bgcolor="AAAAAA">
			<tr>
				<td>
					<center><b>Dashboard</b></center><hr width="100%">
					<form name="getPlaylist" action="index.php" method="POST">
					<center>
					<table width="85%" border=0><tr><td valign="top">
					<?php
					for ($i=0; $i<count($genreNames); $i++) {
						$checked = '';
						if (isset($_POST[$genreNames[$i]])) { $checked = ' checked'; }
						echo "<input type=\"checkbox\" name=\"".$genreNames[$i]."\" value=\"".$genreNames[$i]."\" / ".$checked.">".$genreNames[$i]."<br/>";
						if ($i==4 || $i==9) echo "</td><td valign=\"top\">";
					}
					?>	
					</td></tr>
					</table>
					<tr><td>
					<p><center>
					min year: <input type="text" name="minyear" value="<?php echo $_POST['minyear'] ?>"></input>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					max year: <input type="text" name="maxyear" value="<?php echo $_POST['maxyear'] ?>"></input>
					<p>
					</center></td></tr>
					<tr><td>
				 	<p><center>
					min score: <input type="text" name="minprob" value="<?php echo $theMinScore?>"></input>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<!--- limit: <input type="text" name="limit" value="100"></input> --->
					</center></td></tr>
					<tr><td>
					<p><center>
					<p><br/>
					<input type="submit" style="font-size: larger" name="classifier" value="Sad" />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="submit" style="font-size: larger" name="classifier" value="Happy" />
					<p>&nbsp;
					</form>					
					</center></td></tr>
					</center>
				</td>
			</tr>
		</table>	
	</center>
	<?php
}


function getTrack($query, $classifier, $limit, $genreNames) {
	$genres = 'in all genres';
	$genSelection = array();
	for ($i=0; $i<count($genreNames); $i++) 
		if (isset($_POST[$genreNames[$i]])) $genSelection[] = $genreNames[$i];
	if (count($genSelection) > 0)	$genres = 'with genre '.implode(' / ',$genSelection);
	$years = 'in any year';
	$minyear = $_POST['minyear'];
	$maxyear = $_POST['maxyear'];
	if ($minyear != '' && $minyear != '') 
		$years = 'between '.$minyear.' and '.$maxyear;
	
	$link = mysql_connect("radiodb.sourcetone.com", "ekogan", "gethungry1") or
        die("Could not connect: " . mysql_error());
    mysql_select_db("ratemood_development", $link);
    
	$result = mysql_unbuffered_query($query, $link);
	$tracks = array();
	$count = 0;
	while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		if ($count < $limit) {
			$tracks[] = array($row['songvectors_id'], $row['genre'], $row['subgenre'], 
						  	  $row['valence'], $row['arousal'], $row['path'], 
						      $row['sad'], $row['happy'], $row['year']);
		}
		$count++;
	}
	mysql_free_result($result);
	mysql_close($link);

	if (count($tracks)>0) {
		$randidx = rand(0, count($tracks));
		$track = $tracks[$randidx];
	}

	?>	
	<center>
	<table width="750" border=0 bgcolor="BBBBBB">
		<tr><td><center>
		<b>Search</b>: <?php echo $classifier ?> songs <?php echo $genres ?> <?php echo $years ?>, with minimum score <?php echo $_POST['minprob']; ?>
		<br/>
		<?php 
			echo "Found ".$count." songs.";
			if (count($tracks)>0)
				echo " Picked song with rank ".$randidx." (max ".$limit.").";
			else
				echo " Try lowering minimum score.";
		?>
		</td></tr>
	</table>					
	</center>		

	<?php 
	if (count($tracks)==0)
		return NULL;
	else
		return $track;
}

?>