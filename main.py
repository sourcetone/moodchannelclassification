import re
import csv
import numpy as np
from sklearn import preprocessing
#from scikits.learn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegression
from sklearn.cross_validation import StratifiedKFold
from sklearn.grid_search import GridSearchCV
import random
from sklearn import svm

path = '/Users/Gene/Sourcetone/data/'

def get_test_set():
	test_tracks = []
	test_features = []
	tracks = csv.reader(open(path+'round3_features.csv', 'r'), delimiter=',')
	tracks.next()	# skip header
	for track in tracks:
		url = re.sub('/home/sourcetone/mp3lib', 'http://admin.sourcetone.com/songs_mp3', track[0])
		sv_id = int(track[1])
		features = track[2:]
		for i in range(0, len(features)):	features[i] = float(features[i])
		test_tracks.append([url, sv_id])
		test_features.append(features)
	test_features = np.array(test_features)	
	return (test_tracks, test_features)

def get_ground_truth(classifier):
	features = {}		# get features first
	#feat_gt = csv.reader(open(path+'original_features.csv', 'r'), delimiter=',')
	feat_gt = csv.reader(open('/Users/Gene/sourcetone/trainSad/data/original_features.csv', 'r'), delimiter=',')	
	feat_gt.next()
	for track in feat_gt:
		sv_id = int(track[0])
		feat = track[1:]
		for i in range(0, len(feat)):	feat[i] = float(feat[i])
		features[int(sv_id)] = feat	
	gt_tracks = []		# get current results and append features if available
	gt_features = []
	#results = csv.reader(open(path+'ground_truth.csv', 'r'), delimiter=',')
	results = csv.reader(open('/Users/Gene/sourcetone/trainSad/data/ground_truth.csv', 'r'), delimiter=',')
	results.next()
	for (idx, songvectors_id, audio_path, classifier_round, classifier0, prediction, result, labeler) in results:
		audio_path = re.sub('/home/sourcetone/mp3lib', 'http://admin.sourcetone.com/songs_mp3', audio_path)
		if classifier0==classifier:
			try:
				feat = features[int(songvectors_id)]
				if result=='true' or result=='false':
					gt_tracks.append([int(songvectors_id), int(classifier_round), classifier0, result, labeler])
					gt_features.append(feat)
			except:	pass
	return (gt_tracks, gt_features)

def normalize_features(feat1, feat2):	
	feat = np.concatenate([feat1, feat2])
	#feat_norm = preprocessing.normalize(feat, norm='l2')
	for i in range(feat.shape[1]):
		feat[:,i] = (feat[:,i]-np.min(feat[:,i]))/(np.max(feat[:,i])-np.min(feat[:,i]))
	feat1 = feat[0:len(feat1)]
	feat2 = feat[len(feat1):]
	return (feat1, feat2)
	
def train_logistic_regression(tracks, features):
	labels = []
	for (sv_id, classifier_round, classifier, result, labeler) in tracks:
		if result=='true':		labels.append(1)
		elif result=='false':	labels.append(0)
	labels = np.array(labels)
	param_grid = {'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000] }
	clf = GridSearchCV(LogisticRegression(penalty='l2'), param_grid)
	clf = clf.fit(features, labels)
	print "Best estimator found by grid search:"
	print clf.best_estimator_
	return clf

def write_sql_file(filename, classifier, tracks, prob, min_prob):
	writefile = open(path+filename,'w')
	for i in range(0, len(tracks)):
		(url, sv_id) = tracks[i]
		p = prob[i]
		if p[1]>min_prob:
			values = str(sv_id)+',"'+url+'",'+str(3)+',"'+classifier+'",'+str(p[1])+', NULL, NULL'
			query = 'INSERT INTO sctn.classifier_training (songvectors_id, audio_path, classifier_round, classifier, prediction, result, labeler) VALUES ('+values+');'
			writefile.write(query+'\n')

###### new #########	
def get_track_info():
	tracks = csv.reader(open('/Users/Gene/sourcetone/trainSad/final_demo/data/test_tracks.csv', 'r'), delimiter='|')	
	tracks.next()
	for track in tracks:
		f_id = int(track[0])
		feat = track[1:160]
		for i in range(0, len(feat)):	feat[i] = float(feat[i])
		val
		aro
		path_id
		gen
		subgen
	

# sad
(gt_tracks, gt_features) = get_ground_truth('sad')
(test_tracks, test_features) = get_test_set()
(gt_features, test_features) = normalize_features(gt_features, test_features)
classifier = train_logistic_regression(gt_tracks, gt_features)
prob_sad = classifier.predict_proba(test_features)
write_sql_file('pred_sad_sql.txt', 'sad', test_tracks, prob_sad, 0.78)

# happy
(gt_tracks, gt_features) = get_ground_truth('happy')
(test_tracks, test_features) = get_test_set()
(gt_features, test_features) = normalize_features(gt_features, test_features)
classifier = train_logistic_regression(gt_tracks, gt_features)
prob_happy = classifier.predict_proba(test_features)
write_sql_file('pred_happy_sql.txt', 'happy', test_tracks, prob_happy, 0.8)


#labels = []
#for (sv_id, classifier_round, classifier, result, labeler) in gt_tracks:
#	if result=='true':		labels.append(1)
#	elif result=='false':	labels.append(0)
#labels = np.array(labels)
#param_grid = {'C': [0.001, 0.01, 0.1, 1, 10, 100, 1000] }
#clf = GridSearchCV(LogisticRegression(penalty='l2'), param_grid)
#clf = clf.fit(gt_features, labels)
#print "Best estimator found by grid search:"
#print clf.best_estimator
