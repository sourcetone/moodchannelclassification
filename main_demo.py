import csv
import numpy as np
import re
from sklearn.grid_search import GridSearchCV
from sklearn.linear_model import LogisticRegression

path = '/Users/Gene/Sourcetone/trainSad/demo_data/'


# TEST SET (1 mil tracks to label)
# ================================
# need: feat1, feat2, ... featn
#  - columns: track_id, __, feat x 160
# need: tracks1M.csv
#  - columns: track_id, val, aro, popularity, year, genre, url???
#  - internap: SELECT * FROM sctn.pl

def get_test_set():	
	features = {}		# get features
	idx = 0
	for idx_file in range(1, 11):
		print idx_file
		tracks = csv.reader(open(path+'feat'+str(idx_file)+'.csv', 'r'), delimiter=',')
		tracks.next()		# skip header	
		for track in tracks:
			track_id = int(track[0])
			try:
				feat = []
				for (i,t) in enumerate(track[2:]):
					feat.append(float(t))
				features[track_id] = feat
				idx += 1
			except:
				pass
	test_tracks = []
	test_features = []
	tracks = csv.reader(open(path+'tracks1M.csv', 'r'), delimiter=',')
	tracks.next()	# skip header
	for track in tracks:
		track_id = int(track[0])
		try:
			feat = features[track_id]
			val = float(track[1])
			aro = float(track[2])
			popularity = float(track[3])
			year = int(track[4])
			genre = int(track[5])
			url = 'None'
			#url = re.sub('/home/sourcetone/mp3lib', 'http://admin.sourcetone.com/songs_mp3', track[0])	
			test_tracks.append([track_id, val, aro, popularity, year, genre, url])
			test_features.append(feat)
		except:
			pass
			#print "no feat for " + str(track_id)
	test_features = np.array(test_features)	
	return (test_tracks, test_features)



# GROUND TRUTH
# ============
# need: ground_truth_features.csv
#   - columns: songvectors_id, feat x 160
#   - dreamhost: SELECT * FROM ratemood_development.feature_vectors2
# need: ground_truth.csv
#   - columns: id,songvectors_id,audio_path,classifier_round,classifier,prediction,result,labeler
#   - internap: SELECT * from sctn.classifier_training WHERE songvectors_id IS NOT NULL AND result IS NOT NULL
# todo: get ~558 missing songvectors_id in classifier_training ground truth tab


# getting features for sctn.classifier_training
#SELECT af.id, af.songs_id, fv.*, CAST(af.path AS CHAR(10000) CHARACTER SET utf8)
#FROM ratemood_development.audiofiles as af
#INNER JOIN ratemood_development.songvectors2 as sv
#ON sv.audiofiles_id = af.id
#INNER JOIN ratemood_development.feature_vectors2 as fv
#ON fv.songvectors_id = sv.id
#WHERE fv.songvectors_id IN (2432,

def get_ground_truth(classifier):
	features = {}		# get features first
	feat_gt = csv.reader(open(path+'ground_truth_features.csv', 'r'), delimiter=',')
	feat_gt.next()
	for track in feat_gt:
		sv_id = int(track[0])
		feat = track[1:]
		for i in range(0, len(feat)):	feat[i] = float(feat[i])
		features[int(sv_id)] = feat	
	gt_tracks = []		# get current results and append features if available
	gt_features = []
	results = csv.reader(open(path+'ground_truth.csv', 'r'), delimiter=',')
	results.next()
	for (idx, songvectors_id, audio_path, classifier_round, classifier0, prediction, result, labeler) in results:
		audio_path = re.sub('/home/sourcetone/mp3lib', 'http://admin.sourcetone.com/songs_mp3', audio_path)
		if classifier0==classifier:
			feat = features[int(songvectors_id)]			
			if result=='true' or result=='false':
				if prediction=='NULL':	prediction = -1.0
				gt_tracks.append([int(songvectors_id), int(classifier_round), classifier0, result, float(prediction), labeler])
				gt_features.append(feat)
	gt_features = np.array(gt_features)
	return (gt_tracks, gt_features)

def get_evened_out_ground_truth(gt_tracks, gt_features, classifier):
	# even out set
	num_pos = len([g for g in gt_tracks if g[3]=='true'])
	num_neg = len([g for g in gt_tracks if g[3]=='false'])
	num_extra = num_pos - num_neg
	(h_tracks, h_features) = get_ground_truth(classifier)
	# grab num_extra true positive tracks from happy and append them to gt as negative sad
	idx = [ i for (i,h) in enumerate(h_tracks) if h[3]=='true' ]
	h_tracks = [ h_tracks[i] for i in idx ]
	h_feat = [ h_features[i] for i in idx ]
	idx = [i[0] for i in sorted(enumerate(h_tracks), key=lambda x:x[1][4])]
	h_tracks = [ h_tracks[i] for i in idx ]
	h_feat = [ h_feat[i] for i in idx ]
	for i in range(0, num_extra):
		h_tracks[-i][3] = 'false'	#turn to opposite
		h_tracks[-i][4] = 1.0 - h_tracks[-i][4]
		gt_tracks.append(h_tracks[-i])
		gt_features.append(h_feat[-i])
	return (gt_tracks, gt_features)

def standardize_features(feat1, feat2):	
	feat = np.concatenate([feat1, feat2])
	for i in range(feat.shape[1]):
		feat[:,i] = (feat[:,i]-np.mean(feat[:,i]))/np.std(feat[:,i])
	feat1 = feat[0:len(feat1)]
	feat2 = feat[len(feat1):]
	return (feat1, feat2)
	
def normalize_features(feat1, feat2):	
	feat = np.concatenate([feat1, feat2])
	#feat_norm = preprocessing.normalize(feat, norm='l2')
	for i in range(feat.shape[1]):
		feat[:,i] = (feat[:,i]-np.min(feat[:,i]))/(np.max(feat[:,i])-np.min(feat[:,i]))
	feat1 = feat[0:len(feat1)]
	feat2 = feat[len(feat1):]
	return (feat1, feat2)

def train_logistic_regression(tracks, features):
	labels = []
	for (sv_id, classifier_round, classifier, result, pred, labeler) in tracks:
		if result=='true':		labels.append(1)
		elif result=='false':	labels.append(0)
	labels = np.array(labels)
	param_grid = {'C': [0.001, 0.002, 0.005, 0.01, 0.02, 0.05, 0.1, 0.2, 0.3, 0.5, 0.7, 0.8, 1, 1.5, 2, 5, 7, 10, 100, 1000] }
	#clf = GridSearchCV(LogisticRegression(penalty='l2'), param_grid)
	clf = LogisticRegression(penalty='l2', C=0.1)
	clf = clf.fit(features, labels)
	print "Best estimator found by grid search:"
	return clf

def write_sql_file(filename, classifier, tracks, prob, min_prob):
	# [track_id, val, aro, popularity, year, genre, url]
	writefile = open(path+filename,'w')
	#[track_id, val, aro, popularity, year, genre, url])
	for i in range(0, len(tracks)):
		track_id = tracks[i][0]
		p = prob[i][1]
		if p>min_prob:
			values = str(track_id)+','+str(p)
			query = 'INSERT INTO sctn.classifier_sad_demo (track_id,'+classifier+') VALUES ('+values+');'
			writefile.write(query+'\n')


# sad
def classify_sad():
	(test_tracks, test_features) = get_test_set()
	(gt_tracks, gt_features) = get_ground_truth('sad')
	#(gt_tracks, gt_features) = get_evened_out_ground_truth(gt_tracks, gt_features, 'happy')
	(gt_features, test_features) = normalize_features(gt_features, test_features)
	#(gt_features, test_features) = standardize_features(gt_features, test_features)
	classifier = train_logistic_regression(gt_tracks, gt_features)
	prob_sad = classifier.predict_proba(test_features)
	write_sql_file('pred_sad_sql.txt', 'sad', test_tracks, prob_sad, 0.8)


# happy
def classify_happy():
	(test_tracks, test_features) = get_test_set()
	(gt_tracks, gt_features) = get_ground_truth('happy')
	#(gt_tracks, gt_features) = get_evened_out_ground_truth(gt_tracks, gt_features, 'happy')
	(gt_features, test_features) = normalize_features(gt_features, test_features)
	#(gt_features, test_features) = standardize_features(gt_features, test_features)
	classifier = train_logistic_regression(gt_tracks, gt_features)
	prob_sad = classifier.predict_proba(test_features)
	write_sql_file('pred_happy_sql.txt', 'happy', test_tracks, prob_sad, 0.8)


classify_sad()